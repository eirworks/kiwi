<?php 
!defined('KIWI') ? define('KIWI', 'KIWIWIKI') : null;

require dirname(__FILE__)."/kiwi.php";

if ($_SERVER['REQUEST_METHOD'] == "POST")
{
	// Save page if content is not empty
	// then redirect to view this page

	if (isset($_POST['content']))
	{
		if (!empty($_POST['content']))
		{
			$new_content = $_POST['content'];
			unlink($path_to_page);
			file_put_contents($path_to_page, $new_content);
			header("Location: /?p=".$title);
			exit;
		}
		else {
			// if content is empty, delete the page.
			if (file_exists($path_to_page))
			{
				unlink($path_to_page);
			}
		}
	}
}

$exist = false;

if (file_exists($path_to_page))
{
	$exist = true;

	$raw_content = file_get_contents($path_to_page);

	$content = preg_replace("/\?([A-Za-z0-9\_\-]+)/", "<a href=\"?p=$1\">$1</a>", $raw_content);
}
else {
	$content = "";
}

require "kiwi/edit.php";