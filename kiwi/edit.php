<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>
	<style>
		a, a:visited, a:active, a:focus {
			color:blue;
		}

		main.kiwi-main-container {
			width: 960px;
			margin:auto;
		}

		nav.kiwi-main-nav {
			margin-bottom: 10px;
		}

		nav.kiwi-main-nav a {
			padding:2px;
			margin:0 6px;
		}

		nav.kiwi-main-nav a.kiwi-brand {
			padding:2px 10px;
			background-color: blue;
			color: white;
			font-weight: bold;
			margin-left: 0;
			margin-right: 0;
			text-decoration: none;
		}

		nav.kiwi-main-nav a.kiwi-brand span.kiwi-brand-subtitle
		{
			font-style: italic;
			font-weight: normal;
		}

		h1.kiwi-page-not-exist {
			color: darkred;
		}

		textarea {
			display: block;
			width:100%;
			min-height: 200px;
		}
	</style>
</head>
<body>
	<main class="kiwi-main-container">
		<nav class="kiwi-main-nav">
			<a href="/" class="kiwi-brand">
				<?php echo $config['wiki_title'] ?> 
				<span class="kiwi-brand-subtitle">~ <?php echo $config['wiki_subtitle'] ?></span>
			</a>
			<a href="/?p=<?php echo $title ?>">view</a>
		</nav>

		<h1>Editing <?php echo $title ?></h1>

		<?php if(!$exists): ?>
			<p>Page <?php echo $title ?> is not exist, clicking save button will create this page.</p>
		<?php endif ?>

		<form action="" method="post">
			<textarea name="content"><?php echo $raw_content ?></textarea>
			<p>
				<button type="submit">Save</button>
				<a href="/?p=<?php echo $title ?>">Cancel</a>
			</p>
			<p>Leave content empty if you want to delete this page.</p>
		</form>	
	</main>
</body>
</html>