<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>
	<style>
		a, a:visited, a:active, a:focus {
			color:blue;
		}

		main.kiwi-main-container {
			width: 960px;
			margin:auto;
		}

		nav.kiwi-main-nav {
			margin-bottom: 10px;
		}

		nav.kiwi-main-nav a {
			padding:2px;
			margin:0 6px;
		}

		nav.kiwi-main-nav a.kiwi-brand {
			padding:2px 10px;
			background-color: blue;
			color: white;
			font-weight: bold;
			margin-left: 0;
			margin-right: 0;
			text-decoration: none;
		}

		nav.kiwi-main-nav a.kiwi-brand span.kiwi-brand-subtitle
		{
			font-style: italic;
			font-weight: normal;
		}

		h1.kiwi-page-not-exist {
			color: darkred;
		}
	</style>
</head>
<body>
	<main class="kiwi-main-container">
		<nav class="kiwi-main-nav">
			<a href="/" class="kiwi-brand">
				<?php echo $config['wiki_title'] ?> 
				<span class="kiwi-brand-subtitle">~ <?php echo $config['wiki_subtitle'] ?></span>
			</a>
			<?php if($config['allow_edit']): ?>
				<a href="edit.php?p=<?php echo $page ?>">edit</a>
			<?php endif; ?>
		</nav>

		<h1 <?php echo !$exists ? 'class="kiwi-page-not-exist"' : '' ?>><?php echo $title ?></h1>

		<?php echo $content ?>		
	</main>
</body>
</html>