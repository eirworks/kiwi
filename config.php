<?php 

return [

	'main_page' => 'HelloWorld',
	'wiki_title' => 'Kiwi - Wiki',
	'wiki_subtitle' => 'Your personal wiki',

	'allow_edit' => true,
];
