<?php 

if (!defined('KIWI')): die("No access!"); endif;

!defined('KIWI_CONFIG_FILENAME') ? define('KIWI_CONFIG_FILENAME', 'config.php') : null;

if (!file_exists(KIWI_CONFIG_FILENAME))
{
	die("ERROR: Missing configuration file!");
}

$config = require KIWI_CONFIG_FILENAME;

$param_page = empty($_GET['p']) ? $config['main_page'] : $_GET['p'];

$page = preg_replace("/[^0-9a-zA-Z\_\-]/", "", $param_page);

$path_to_page = dirname(__FILE__)."/data/".$page.".wiki";

$title = $page;