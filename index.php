<?php 
!defined('KIWI') ? define('KIWI', 'KIWIWIKI') : null;
require dirname(__FILE__)."/kiwi.php";

$exists = false;

if (file_exists($path_to_page))
{
	$exists = true;

	$content = file_get_contents($path_to_page);

	$content = preg_replace("/\?([A-Za-z0-9\_\-]+)/", "<a href=\"?p=$1\">$1</a>", $content);
}
else {
	$title = "Page `$page` not exist";
	$content = "Page `$page` not exist.";
	if ($config['allow_edit'])
	{
		$content .= " You can create it by clicking edit above.";
	}
	else {
		$content .= " Unfortunately, you cannot edit this page right now.";
	}
}


require "kiwi/view.php";
